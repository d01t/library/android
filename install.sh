#!/bin/bash
# Stop on error
set -e
# Install mandatory programs and utilities
export DEBIAN_FRONTEND=noninteractive
curl -sL https://deb.nodesource.com/setup_14.x | bash -
curl -sS https://dl.yarnpkg.com/debian/pubkey.gpg | apt-key add -
apt-get --yes --quiet=2 update
apt-get --yes --quiet=2 install \
	apt-transport-https \
	build-essential \
	ca-certificates \
	curl \
	dnsutils \
	gcc \
	git \
	gradle \
	g++ \
	make \
	nodejs \
	procps \
	rsync \
	ruby \
	ruby-dev \
	wget \
	unzip \
	yarn \
	zip
# Purge unnecessary files
rm -rf /var/lib/apt/lists/*

gem install fastlane
gem install bundle

cat <<EOF >> /opt/tools/entrypoint.sh
#!/bin/bash
if [ -n "\${HOST_USER_ID}" ]
then
	usermod -o -u "\${HOST_USER_ID}" android
	groupmod -o -g "\${HOST_GROUP_ID}" android
fi

function checkbin() {
    type -P su-exec
}

function su_mt_user() {
    su android -c '"$0" "$@"' -- "$@"
}

chown android:android /opt/android-sdk-linux

printenv

if checkbin; then
    exec su-exec android:android /opt/tools/android-sdk-update.sh "$@"
else
    su_mt_user /opt/tools/android-sdk-update.sh ${1}
fi

EOF

chmod +x /opt/tools/entrypoint.sh
