ARG VERSION=latest

FROM runmymind/docker-android-sdk:${VERSION}

ENV JAVA_OPTS "-Xms512m -Xmx1024m"
ENV GRADLE_OPTS "-XX:+UseG1GC -XX:MaxGCPauseMillis=1000"

COPY install.sh /root/

RUN /bin/bash /root/install.sh
